// Clean .git as it slowly gets massive and I need to 
// clean it.

const { exec } = require('child_process');
exec('rm .git -rf', () => {
console.log("Cleaned .git")
});

exec('refresh', () => {
console.log("Cleaned .git")
});
// Restana server
const httpServer = require("./lib/httpServer");
httpServer.run();

// Discord.JS startup.
const Discord = require('discord.js');
const client = new Discord.Client();

// Bot version and prefix
var prefix = "warm!"
var version = "v1 (The Awakening)";

// FS / Filesystem setup.
const fs = require("fs")

// "Economy" enmap
const Enmap = require("enmap");
client.coins = new Enmap({
	name: "coins"
});

// Music queue
client.nqueue = new Map();

// Setup categories for the Help command.
require("./lib/setCategories").set(client, prefix);
client.categories = {};
// When discord.js is ready
client.on('ready', () => {
console.log("We're online! [" + client.user.tag + "]")
// List of activies the bot can use

const activity_list = ["Hello LWCB!", "It is a sunny day outside.", prefix + "help", prefix + "help", version];
// Pick a random activity and set it as
// the running activity
setInterval(()=>{
client.user.setActivity(activity_list[Math.floor(Math.random()*activity_list.length)])
}, 10000)

// Update the HTTP api constantly
setInterval(()=>{
httpServer.updateAPI(client)
})

});
let files = [];

fs.readdir(__dirname + "/commands", function (err, filesx) {
files = filesx;
})

// When a message is recived.
client.on('message', message => {
  
// client.error / client.warn
require("./lib/embed").set(message, client);
	// Test if user is not bot. To prevent infnite loops.
	if (message.author == client.user) return;
	// Get args
	let args = message.content.split(" ").slice(1);
	// Get the commmand
	let requestedCommand = message.content.split(" ").shift().toLowerCase();
	// If a user does not have any coin amount set, add a starting amount of 0,
	// so we can add to the coin amount.
	if (!client.coins.get(message.author.id)) client.coins.set(message.author.id, 0);
	// Add one coin to the user, due to one message.
	client.coins.set(message.author.id, client.coins.get(message.author.id) + 1)
	// Read the Commands dir, to get all file names.
    // Get every filename.
		for(var i = 0, len = files.length; i < len; i++) {
      var name = files[i];
      // Require the requested command
			let command = require("./commands/" + name);
      // Find out the name of the file we're trying to execute,
      // but add our prefix and remove the .js end.
			var jsname = prefix + name.split(".js")[0];
      // Check if the requested command is the command we're 
      // reading.
			if (requestedCommand == jsname) {
          console.log(requestedCommand)
         // Launch the command using, message, args and client.
				command.command(message, args, client)
			}
    }
})
client.on('erorr', error => {
  console.log(error)
})

console.log("logging in")
// Login using Env.
client.login(process.env.token);
