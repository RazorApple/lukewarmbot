fetch('./api/info').then(function(response) {
      response.json().then(function(data) {
        document.getElementById("guilds").innerHTML = data.guilds;
        document.getElementById("users").innerHTML = data.users;
        document.getElementById("channels").innerHTML = data.channels;
      });
    })
