

// Require a native node.js
// libary. (meaning no external libaries)
const https = require('https');
// GetRequest command (pretty much json call)
function getRequest(link, callback) {

	https.get(link, (resp) => {
		let data = '';
		resp.on('data', (chunk) => {
			data += chunk;
		});

		resp.on('end', () => {
			callback(JSON.parse(data));
		});

	}).on("error", (err) => {
		throw err.message;
	});
}

exports.getRequest = getRequest;

