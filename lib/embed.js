

const Discord = require("discord.js")

function set(message, client) {
	// Warn function  across all commands
	client.warn = (i, image) => {
		// Check if text isn't empty,
		// otherwise Discord will throw a error.
		if (!i) throw "No text found";
		const embed = new Discord.MessageEmbed()
			.setTitle(i)
			.setFooter("Warning")
			.setTimestamp()
			.setColor("FFA500")
			.setAuthor(client.user.username, client.user.avatarURL());
		if (!i) throw "No text found";

		// Check if a the image var is a function
		if (typeof image === "function") {
			// And if it is, then return the function of Image
			return image(embed);
		}

		// But if it isn't a function, it's a string
		// and can be added to the embed.
		// And then we send the embed.
		if (image) embed.setImage(image);
		message.channel.send(embed).catch((x) => {
			message.channel.send(`**${i}**
Image: ${image}

__${x.message}__`)
		});

	}
	// Error function across all commands
	client.error = (i, image) => {
		const embed = new Discord.MessageEmbed()
			.setTitle(i)
			.setFooter("Failure")
			.setTimestamp()
			.setColor("FF0000")
			.setAuthor(client.user.username, client.user.avatarURL());

		if (!i) throw "No text found";

		// Check if a the image var is a function
		if (typeof image === "function") {
			// And if it is, then return the function of Image
			return image(embed);
		}

		// But if it isn't a function, it's a string
		// and can be added to the embed.
		// And then we send the embed.
		if (image) embed.setImage(image);
		message.channel.send(embed).catch((x) => {
			message.channel.send(`**${i}**
Image: ${image}

__${x.message}__`)
		});
	}
	// Info function accross all commands
	client.info = (i, image) => {
		const embed = new Discord.MessageEmbed()
			.setTitle(i)
			.setFooter("Sucess")
			.setTimestamp()
			.setColor("00FF00")
			.setAuthor(client.user.username, client.user.avatarURL());

		if (!i) throw "No text found";

		// Check if a the image var is a function
		if (typeof image === "function") {
			// And if it is, then return the function of Image
			return image(embed);
		}

		// But if it isn't a function, it's a string
		// and can be added to the embed.
		// And then we send the embed.
		if (image) embed.setImage(image);
		message.channel.send(embed).catch((x) => {
			message.channel.send(`**${i}**
Image: ${image}

__${x.message}__`)
		});
	}


}
exports.set = set;

