

// Webserver based on Restana and serve-static

// Empty API object for /api/info
const api = {};

function updateAPI(client) {
	api["guilds"] = client.guilds.cache.size;
	api["users"] = client.users.cache.size;
	api["channels"] = client.channels.cache.size;
}

function run() {
	// Serve-static + Path
	const files = require('serve-static')
	const path = require('path')

	// Serve the /seedable directory
	const serve = files(path.join("/app/", 'seedable'))

	// Create a restana instance for the API and 
	// handling.
	const app = require('restana')({
		disableResponseEvent: true
	})

	// Send the API object on /api/info inorder to get
	// the channels, guilds and users
	// into the website.
	app.get('/api/info', function (req, res) {
		res.body = api;
		res.send()
	})

	// Use serve-static as the middleware.
	app.use(serve)
	// Host on 3000, Glitch's default port.
	app.start(3000)
}


exports.updateAPI = updateAPI;
exports.run = run;

