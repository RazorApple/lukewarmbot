

function stop(message, serverQueue, client) {
	if (!message.member.voice.channel)
		return client.error("You have to be in a voice channel to stop the music!");
	serverQueue.songs = [];
	serverQueue.connection.dispatcher.end();
	client.info("Stopped.")
}

exports.stop = stop;

