

var http = require("../http");

function search(args, urlresponse) {
	http.getRequest(`https://www.googleapis.com/youtube/v3/search?part=id&key=${process.env.apikey}&q=${args.join(" ")}`, async(json) => {
		var url = `https://www.youtube.com/watch?v=${json.items[0].id.videoId}`
		urlresponse(url)
	})
}

exports.search = search;

