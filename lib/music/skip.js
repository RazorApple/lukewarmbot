

function skip(message, serverQueue, client) {
	if (!message.member.voice.channel)
		return client.error("You have to be in a voice channel to stop the music!");
	if (!serverQueue)
		return client.error("There is no song that I could skip!");
	client.info("Skipped.")
	serverQueue.connection.dispatcher.end();
}

exports.skip = skip;

