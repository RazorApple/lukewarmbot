

const ytdl = require("ytdl-core");

function play(guild, song, client) {
	const serverQueue = client.nqueue.get(guild.id);
	if (!song) {
		serverQueue.voiceChannel.leave();
		client.nqueue.delete(guild.id);
		return;
	}

	const dispatcher = serverQueue.connection
		.play(ytdl(song.url))
		.on("finish", () => {
			serverQueue.songs.shift();
			play(guild, serverQueue.songs[0], client)
		})
		.on("error", error => console.error(error));
	dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
	client.info("Playing: ", (embed) => {
		embed.setDescription(`Start playing: **${song.title}**`)
		serverQueue.textChannel.send(embed);
	})
}

exports.play = play;

