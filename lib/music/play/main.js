

var Discord = require("discord.js");
const ytdl = require("ytdl-core");
var search = require("../searchYT");
var play = require("./api");

async function execute(message, args, client) {
	const serverQueue = client.nqueue.get(message.guild.id);
	const voiceChannel = message.member.voice.channel;
	if (!voiceChannel)
		return client.error("You need to be in a voice channel to play music!");
	const permissions = voiceChannel.permissionsFor(message.client.user);
	if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
		return client.error("I need the permissions to join and speak in your voice channel!");
	}

	search.search(args, async(url) => {


		const songInfo = await ytdl.getInfo(url);
		const song = {
			title: songInfo.title,
			url: songInfo.video_url
		};

		if (!serverQueue) {
			const queueContruct = {
				textChannel: message.channel,
				voiceChannel: voiceChannel,
				connection: null,
				songs: [],
				volume: 5,
				playing: true
			};

			client.nqueue.set(message.guild.id, queueContruct);

			queueContruct.songs.push(song);

			try {
				var connection = await voiceChannel.join();
				queueContruct.connection = connection;
				play.play(message.guild, queueContruct.songs[0], client);
			} catch (err) {
				client.nqueue.delete(message.guild.id);
				return client.error(err);
			}
		} else {
			serverQueue.songs.push(song);
			return client.info(`Added: ${song.title}`);
		}
	})
}

exports.execute = execute;

