// Setup client.categories for use in the help command.
const fs = require("fs")

function set(client, prefix) {
// Read all commands
fs.readdir("/app/commands", function (err, files) {
// If there's a error, scream out the error/
if(err) throw err;
    // Go over every file in the commands dir
		files.forEach((name) => {
      // Get the current command
      let command = require("../commands/" + name);
        // If this category doesn't exist, then add it.
        if(!client.categories[command.category]) client.categories[command.category] = [];
        // Add this command to the category it's set at.
        client.categories[command.category].push(prefix+name.split(".js")[0])
		})
	})
}

exports.set = set;