var Discord = require("discord.js");

function command(message, args, client) {
if (!message.member.hasPermission(["KICK_MEMBERS","ADMINISTRATOR"])) {
return client.error("User has no warn permissions.");
}

let member = message.guild.member(message.mentions.users.first()); 
let reason = args.slice(1).join(" ");

// If 0 arguments
if(!args[0]) {
return client.error("No arguments! Usage: warm!warn [mention/id] [reason]");
}
// Check if the user (mention) doesn't exist
// and if doesn't then try getting the ID user;
if(!member) {
member = message.guild.members.cache.get(args[0]);
member = message.guild.member(member);
}

// If neither the ID user or the Mention user don't 
// exist
if(!member) {
return client.error("This user does not exist!")
}


client.info("Warn: ", (embed) => {
embed.setDescription(`you warned: **${member.user.tag}**
in: **${message.guild.name}**
for: **${reason}**`)
message.channel.send(embed);
})

client.info("You've been warned.", (embed) => {
embed.setDescription(`by: **${message.author.tag}**
in: **${message.guild.name}**
for: **${reason}**`)
member.user.send(embed);
})
}

exports.command = command;
exports.category = "moderation";