var Discord = require("discord.js");
var packages = require("../package.json");


function command(message, args, client) {
var lol = 0;
Object.entries(client.categories).forEach(([key]) => {
lol += key.length;
});

	let totalSeconds = (client.uptime / 1000);
	let days = Math.floor(totalSeconds / 86400);
	let hours = Math.floor(totalSeconds / 3600);
	totalSeconds %= 3600;
	let minutes = Math.floor(totalSeconds / 60);
	let seconds = Math.floor(totalSeconds % 60);
	let uptime = `${days} days, ${hours} hours, ${minutes} minutes and ${seconds} seconds`;
	const time = Math.round(Date.now() - message.createdTimestamp);
const owner = client.guilds.cache.get(process.env.officalserver).owner.user.tag
client.info("Info about Bot", (embed) => {
embed.setDescription(`Latency: **${time}**
Uptime: **${uptime}**
Users: **${client.users.cache.size}**
Guilds: **${client.guilds.cache.size}**
DiscordJS version: **${packages.dependencies["discord.js"].split("").splice(1).join("")}**
Bot created by: **${owner}**
Command Amount: **${lol}**`);
message.channel.send(embed);
})

}

exports.command = command;
exports.category = "bot";
