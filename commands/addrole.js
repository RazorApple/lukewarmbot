var Discord = require("discord.js");

async function command(message, args, client) {

 let userToModify = message.mentions.members.first();
let roleToAdd = message.mentions.roles.first();

if(!userToModify || !roleToAdd) {
userToModify = message.guild.member(client.users.cache.get(args[0]));
roleToAdd = message.guild.roles.cache.get(args[1]);

if(!userToModify || !roleToAdd) {
return client.error(`No user or role found. Please
either ping both role and user, or use it like this:
warm!addrole userid roleid`);
}
}
client.info(`Added the role. Role added: ${roleToAdd.name}`)
userToModify.roles.add(roleToAdd);
}

exports.command = command;
exports.category = "moderation"