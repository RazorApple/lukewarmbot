

var Discord = require("discord.js");

function command(message, args, client) {
	let allCategories = [];

	Object.keys(client.categories).forEach(key => {
		allCategories.push(key)
	});

	if (!args[0]) return client.info(`Help: `, (embed) => {
embed.setDescription(`Use the help command as such: **warm!help (category)**
Category names:
**${allCategories.join(", ")}**`)
message.channel.send(embed)
})
args[0] = args[0].toLowerCase()
	if (!client.categories[args[0]]) return client.error("This category **DOES NOT EXIST**.");
client.info(`${args[0]}: `, (embed)=>{
	embed.setDescription(client.categories[args[0]].join(", "))
	message.channel.send(embed)
})	
}

exports.command = command;
exports.category = "bot";

