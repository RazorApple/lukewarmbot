

var Discord = require("discord.js");
var skip = require("../lib/music/skip")

function command(message, args, client) {
const serverQueue = client.nqueue.get(message.guild.id);

skip.skip(message, serverQueue, client)
}

exports.command = command;
exports.category = "music"