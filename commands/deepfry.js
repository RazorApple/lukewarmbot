var Discord = require("discord.js");
var Jimp = require('jimp');
const isImage = require('is-image');

async function command(message, args, client) {

if(message.attachments.size == 0) {
return client.error("Attach a image!");
}

var Attachment = (message.attachments).array();
var url = Attachment[0].url;

if(!isImage(url)) {
return client.error("This is not a image!");
}
var pixelValue = Math.floor(Math.random() * 2 + 2)
    Jimp.read(url).then(image => {
      image
      .pixelate(pixelValue)
      .contrast(0.95)
      .posterize(8)
      .write('./images/deepfry.png');

message.channel.send("Deepfried: ",{
        files: [
          './images/deepfry.png'
        ]
      })


    }).catch((error)=>{client.erorr(error)})
}

exports.command = command;
exports.category = "pic"