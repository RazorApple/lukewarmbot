var Discord = require("discord.js");

function command(message, args, client) {
let member = message.guild.member(message.mentions.users.first()); 
let reason = args.slice(1).join(" ");

// If 0 arguments
if(!args[0]) {
return client.error("No arguments! Usage: warm!ban [mention/id] [reason]");
}
// Check if the user (mention) doesn't exist
// and if doesn't then try getting the ID user;
if(!member) {
member = message.guild.members.cache.get(args[0]);
member = message.guild.member(member);
}

// If neither the ID user or the Mention user don't 
// exist
if(!member) {
return client.error("This user does not exist!")
}

// Warn if there's no reason, then
// delete it a secound later.
if(!reason) {
return client.warn("No reason.");
}

if (!message.member.hasPermission(["BAN_MEMBERS","ADMINISTRATOR"])) {
return client.error("User has no ban permissions.");
}

// Punish!
member.ban().then((member) => {
message.channel.send("Has " + member.displayName + " been banned.");
}).catch(() => {
client.error("Bot has no kick permsission!")
});
}

exports.command = command;
exports.category = "moderation";
