var Discord = require("discord.js");

function clean(text) {
  if (typeof(text) === "string")
    return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
  else
      return text;
}

function command(message, args, client) {
const owners = ["641691548328656917"];
owners.push(client.guilds.cache.get(process.env.officalserver).ownerID.toString())

if(!owners.includes(message.author.id)) return client.error("You're not a owner.");
    try {
      const code = args.join(" ");
      let evaled = eval(code);
 
      if (typeof evaled !== "string")
        evaled = require("util").inspect(evaled);
 
    client.info("Eval: ", (embed) => {
embed.setDescription(`\`${clean(evaled)}\``)
message.channel.send(embed);
})
    } catch (err) {
    client.info("Eval: ", (embed) => {
embed.setDescription(`\`${clean(err)}\``)
message.channel.send(embed)
    })
    }
}

exports.command = command;
exports.category = "pic";