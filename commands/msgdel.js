var Discord = require("discord.js");

async function command(message, args, client) {
if (!message.member.hasPermission(["MANAGE_MESSAGES","ADMINISTRATOR"])) {
return client.error("User has no message managing permissions.");
}

const id = args[0];

message.channel.messages.fetch(id).then(msg => msg.delete()).catch((err)=>{

if(err.message == "Unknown Message") {
client.error("This message does not exist.");
} else if (err.message) {
client.error("I'm missing permissions.");
}

});
}

exports.command = command;
exports.category = "moderation";