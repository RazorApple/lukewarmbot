var Discord = require("discord.js");

async function command(message, args, client) {
let userToModify = message.mentions.members.first();
let roleToAdd = message.mentions.roles.first();

if(!userToModify || !roleToAdd) {
userToModify = message.guild.member(client.users.cache.get(args[0]));
roleToAdd = message.guild.roles.cache.get(args[1]);

if(!userToModify || !roleToAdd) {
return client.error(`No user or role found. Please
either ping both role and user, or use it like this:
warm!removerole userid roleid`);
}
}

client.info(`Removed the role. Removed: ${roleToAdd.name}`)
userToModify.roles.remove(roleToAdd);
}

exports.command = command;
exports.category = "moderation"