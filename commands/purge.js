var Discord = require("discord.js");

async function command(message, args, client) {
if (!message.member.hasPermission(["MANAGE_MESSAGES","ADMINISTRATOR"])) {
return client.error("User has no message managing permissions.");
}

const amount = args.join(' ');

if (!amount) return client.error("No arguments! Usage: warm!purge [amount]");
if (isNaN(amount)) return client.error("You haven't given a number!")

if (amount > 100) return client.error("You can't delete more than 100 messages!");
if (amount < 1) return client.error("Delete more than one message!");

await message.channel.messages.fetch({ limit: amount }).then(messages => {
    message.channel.bulkDelete(messages)
});
}

exports.command = command;
exports.category = "moderation";