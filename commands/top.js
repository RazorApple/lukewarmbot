/*
			.addField(`${user.tag}`, `${user}`, true)
			.addField("ID:", `${user.id}`, true)
			.addField("Nickname:", `${member.nickname !== null ? `${member.nickname}` : 'None'}`, true)
			.addField("Status:", `${user.presence.status}`, true)
			.addField("In Server", message.guild.name, true)
			.addField("Game:", `${user.presence.game ? user.presence.game.name : 'None'}`, true)
			.addField("Bot:", `${user.bot}`, true)
			.addField("Joined The Server On:", member.joinedAt, true)
			.addField("Account Created On:", user.createdAt, true)
			.addField("Roles:", member.roles.map(roles => `${roles}`).join(', '), true)
*/

// Crazy map Trickery by Alexander_
// Promise stuff by me
var Discord = require("discord.js");
var loopIndex = 0;
let username;

async function command(message, args, client) {
	let array = [];
	let result = new Map();
  client.info("Top:", async (embed)=>{

	for (let [points, userid] of client.coins) {
		array.push([points, userid])
	}

	array.sort((a, b) => a[0] - b[0]);

	for (let _array of array) {
		result.set(_array[0], _array[1])
	}

	for (let [k, v] of result) {
		loopIndex++
		await client.users.fetch(k).then((x) => {
			username = `${x.username}#${x.discriminator}`
		})

		embed.addField(`**${loopIndex}.** ${username}`, `has ${v} points`, true)
		if (loopIndex == 10) break;
	}

	message.channel.send(embed)
  })
}
exports.command = command;
exports.category = "economy";
