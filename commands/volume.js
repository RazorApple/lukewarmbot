var Discord = require("discord.js");


function command(message, args, client) {
const serverQueue = client.nqueue.get(message.guild.id);

  if (!message.member.voice.channel)
    return client.error("You have to be in a voice channel to change the volume!!");
  if (!serverQueue)
    return client.error("There's no song playing!");

args[0] = Number(args[0])
if(args[0] <= 1|| args[0] > 100) return client.error("Bigger than 1, smaller than 100");
serverQueue.connection.dispatcher.setVolume((args[0]/100))
client.info(`Set Volume to: ${args[0]}!`)
}

exports.command = command;
exports.category = "music"