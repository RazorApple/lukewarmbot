var Discord = require("discord.js");

function command(message, args, client) {
	const time = Math.round(Date.now() - message.createdTimestamp);
client.info(`**API Ping**: \`${time}\``)
}

exports.command = command;
exports.category = "bot";
