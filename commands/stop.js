var Discord = require("discord.js");
var stop = require("../lib/music/skip");

function command(message, args, client) {
const serverQueue = client.nqueue.get(message.guild.id);

stop.stop(message, serverQueue, client)
}

exports.command = command;
exports.category = "music"