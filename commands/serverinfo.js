var Discord = require("discord.js")

function command(message, args, client) {
let roles = []; 
message.guild.roles.cache.forEach((x)=>{
roles.push(x.name)
}); 

client.info(`Name: ${message.guild.name}`, (embed)=>{
  embed.addFields(
		{ name: 'Roles: ', value: `\`${roles.join(", ")}\``, inline: true},
    { name: `Server's region:`, value: message.guild.region.split("")[0].toUpperCase() + message.guild.region.split("").splice(1).join(""), inline: true},
    { name: 'Member Count: ', value: message.guild.memberCount, inline: true}
	)
message.channel.send(embed)
});
}

exports.command = command;
exports.category = "bot";
